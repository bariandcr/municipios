package edu.uclm.esi.municipios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.municipios.model.Header;

@Repository
public interface HeaderRepository extends JpaRepository <Header, String> {

}
