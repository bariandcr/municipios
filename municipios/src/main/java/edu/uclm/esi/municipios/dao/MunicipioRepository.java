package edu.uclm.esi.municipios.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.municipios.model.Municipio;

@Repository
public interface MunicipioRepository extends JpaRepository <Municipio, String> {

	@Query(value = "select id, nombre from municipio where provincia_nombre= :p order by nombre", nativeQuery = true)
	List<IMunicipio> findByProvincia(String p);

}
