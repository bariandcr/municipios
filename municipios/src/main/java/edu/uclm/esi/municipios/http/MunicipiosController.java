package edu.uclm.esi.municipios.http;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.esi.municipios.dao.CCAARepository;
import edu.uclm.esi.municipios.dao.HeaderRepository;
import edu.uclm.esi.municipios.dao.IMunicipio;
import edu.uclm.esi.municipios.dao.MunicipioRepository;
import edu.uclm.esi.municipios.dao.ProvinciaRepository;
import edu.uclm.esi.municipios.model.CCAA;

@RestController
@RequestMapping("municipios")
//@CrossOrigin("*")
public class MunicipiosController extends CookiesController {
	@Autowired
	private MunicipioRepository mDao;
	@Autowired 
	private ProvinciaRepository pDao;
	@Autowired
	private CCAARepository cDao;
	@Autowired
	private HeaderRepository hDao;
	
	@GetMapping(value = "/ccaa")
	@ResponseBody
	public List<CCAA> getCCAA(HttpServletRequest request, HttpServletResponse response) {
		String origin = request.getHeader("Origin");
		if (!allowed(origin))
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No estás en los orígenes autorizados");
		response.setHeader("Access-Control-Allow-Origin", origin);
		List<CCAA> ccaa = cDao.findAll();
		return ccaa;
	}
	
	@GetMapping(value = "/provincias/{ca}")
	@ResponseBody
	public List<String> getProvincias(HttpServletRequest request, HttpServletResponse response, @PathVariable String ca) {	
		String caPrevia = super.findCookie(request, "ca");
		System.out.println(caPrevia);
		String origin = request.getHeader("Origin");
		if (!allowed(origin))
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No estás en los orígenes autorizados");
		response.setHeader("Access-Control-Allow-Origin", origin);
		response.setHeader("Access-Control-Allow-Credentials", "true");
		Cookie cookie = new Cookie("ca", ca.replace(" ", ""));
		cookie.setMaxAge(30*24*60*60);
		response.addCookie(cookie);
		return pDao.findByCCAA(ca);
	}
	
	@GetMapping(value = "/municipios/{p}")
	public List<IMunicipio> getMunicipios(HttpServletRequest request, HttpServletResponse response, @PathVariable String p) {
		String origin = request.getHeader("Origin");
		if (!allowed(origin))
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No estás en los orígenes autorizados");
		response.setHeader("Access-Control-Allow-Origin", origin);
		response.setHeader("Access-Control-Allow-Credentials", "true");
		return mDao.findByProvincia(p);
	}

	private boolean allowed(String origin) {
		if (origin==null)
			return true;
		return hDao.findById(origin).isPresent();
	}
}
