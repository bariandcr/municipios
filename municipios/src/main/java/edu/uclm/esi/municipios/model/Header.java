package edu.uclm.esi.municipios.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Header {
	@Id
	private String header;
	
	public String getHeader() {
		return header;
	}
	
	public void setHeader(String header) {
		this.header = header;
	}
}
