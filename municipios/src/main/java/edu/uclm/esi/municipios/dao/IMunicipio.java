package edu.uclm.esi.municipios.dao;

public interface IMunicipio {
	String getId();
	String getNombre();
}
