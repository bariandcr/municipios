package edu.uclm.esi.municipios.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CCAA {
	@Id
	private String nombre;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
