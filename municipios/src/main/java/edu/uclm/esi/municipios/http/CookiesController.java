package edu.uclm.esi.municipios.http;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public abstract class CookiesController {
	private Cookie findCookie(Cookie[] cookies, String cookieName) {
		if (cookies==null)
			return null;
		for (Cookie cookie : cookies)
			if (cookie.getName().equals(cookieName))
				return cookie;
		return null;
	}
	
	protected String findCookie(HttpServletRequest request, String cookieName) {
		Cookie cookie = this.findCookie(request.getCookies(), cookieName);
		return cookie!=null ? cookie.getValue() : null;
	}
}
