package edu.uclm.esi.municipios.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.municipios.model.Provincia;

@Repository
public interface ProvinciaRepository extends JpaRepository <Provincia, String> {

	@Query(value = "select nombre from provincia where ccaa_nombre= :ca order by nombre", nativeQuery = true)
	List<String> findByCCAA(String ca);

}
