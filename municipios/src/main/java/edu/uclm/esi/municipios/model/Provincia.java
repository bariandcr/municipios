package edu.uclm.esi.municipios.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Provincia {
	@Id
	private String nombre;
	
	@OneToOne
	private CCAA ccaa;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public CCAA getCcaa() {
		return ccaa;
	}

	public void setCcaa(CCAA ccaa) {
		this.ccaa = ccaa;
	}
	
	
}
