package edu.uclm.esi.municipios.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.UUID;

public class Broker {
	public static void main(String[] args) {
		new Broker().save();
	}
	
	public void save() {
		int contador = 1;
		String fn = "/Users/macariopolousaola/Downloads/RELACION_MUNICIPIOS_RUSECTOR_AGREGADO_ZONA_CAMPAÑA 2020.csv";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			Connection bd = DriverManager.getConnection("jdbc:mysql://alarcosj.esi.uclm.es:3306/municipios?serverTimezone=UTC",
					"macario.polo", "*********");
			String sqlCA="insert into ccaa (nombre) values (?)";
			String sqlProvincia="insert into provincia (nombre, ccaa_nombre) values (?, ?)";
			String sqlMunicipio="insert into municipio (id, nombre, provincia_nombre) values (?, ?, ?)";
			
			BufferedReader br = new BufferedReader(new FileReader(fn));
			String linea = br.readLine();
			linea = br.readLine();
			while(linea != null) {
				String[] tokens = linea.split(";");
				String ca = tokens[1];
				String provincia = tokens[3];
				String municipio = tokens[6];
				
				PreparedStatement st = null;
				try {
					st = bd.prepareStatement(sqlCA);
					st.setString(1, ca);
					st.executeUpdate();
				} catch (Exception e) {
					//e.printStackTrace();
				}
				
				try {
					st = bd.prepareStatement(sqlProvincia);
					st.setString(1, provincia);
					st.setString(2, ca);
					st.executeUpdate();
				} catch (Exception e) {
					//e.printStackTrace();
				}
				
				try {
					st = bd.prepareStatement(sqlMunicipio);
					st.setString(1, UUID.randomUUID().toString());
					st.setString(2, municipio);
					st.setString(3, provincia);
					st.executeUpdate();
				} catch (Exception e) {
					//e.printStackTrace();
				}		
				System.out.println(contador++);

				linea = br.readLine();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
